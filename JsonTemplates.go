package fnConnect

import "encoding/json"

//the structs in this folder are the JSON data formats for talking with fn server

type deployUpdateSend struct {
	Image       string `json:"image"`
	Memory      int    `json:"memory"`
	Timeout     int    `json:"timeout"`
	IdleTimeout int    `json:"idle_timeout"`
}

type deployPostSend struct {
	Name        string `json:"name"`
	AppID       string `json:"app_id"`
	Image       string `json:"image"`
	Memory      int    `json:"memory"`
	Timeout     int    `json:"timeout"`
	IdleTimeout int    `json:"idle_timeout"`
}

type deployReturnData struct {
	Name        string          `json:"name"`
	AppID       string          `json:"app_id"`
	ID          string          `json:"id"`
	Image       string          `json:"image"`
	Memory      int             `json:"memory"`
	Timeout     int             `json:"timeout"`
	IdleTimeout int             `json:"idle_timeout"`
	CreatedAt   string          `json:"created_at"`
	UpdatedAt   string          `json:"updated_at"`
	Config      json.RawMessage `json:"config"`
}

type postApp struct {
	Name string `json:"name"`
}

type appData struct {
	ID        string `json:"id"`
	Name      string `json:"name"`
	SyslogUrl  string `json:"syslog_url,omitempty"`
	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

type returnListApps struct {
	Items      []appData `json:"items"`
	NextCursor string    `json:"next_cursor,omitempty"`
}

type fnData struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	AppID       string `json:"app_id"`
	Image       string `json:"image"`
	Memory      int    `json:"memory"`
	Timeout     int    `json:"timeout"`
	IdleTimeout int    `json:"idle_timeout"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type returnListFns struct {
	Items      []fnData `json:"items"`
	NextCursor string   `json:"next_cursor,omitempty"`
}
