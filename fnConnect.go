package fnConnect

import (
	"fmt"
	a8util "gitlab.com/autom8.network/go-a8-util"
	pb "gitlab.com/autom8.network/go-a8-util/proto"
)

// Fn holds data about the FN server that fnConnect is talking to and acts as a base for the fnConnect class
type Fn struct {
	Domain         string
	Port           int
	Protocol       string
}

// CallFn processes an A8Command through fn
func (fn Fn) CallFn(request *pb.A8Request) *pb.A8Response {
	var responseData = pb.A8Response{}.Response

	var err error

	switch request.Request.(type) {
	case *pb.A8Request_Show:
		responseData, err = fn.getNamespaceInfoCommand(request.GetShow())

	case *pb.A8Request_Deploy:
		responseData, err = fn.deployFunctionCommand(request.GetDeploy())

	case *pb.A8Request_Invoke:
		responseData, err = fn.invokeFunctionCommand(request.GetInvoke())

	case *pb.A8Request_Logger:
		err = fn.loggerFunctionCommand(request.GetLogger())

	default:
		a8util.Log.Warn("Unknown type as A8Request")
		return nil
	}

	//if an error occurs send it back towards caller
	if err != nil {
		errMsg := fmt.Sprintf("%s", err)

		responseData = &pb.A8Response_Error{
			Error: &pb.Error{
				Message: errMsg,
			},
		}
		a8util.Log.Error("error occured in fnConnect ", errMsg)
	}

	return &pb.A8Response{
		Response: responseData,
	}
}
