package fnConnect

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	a8util "gitlab.com/autom8.network/go-a8-util"
	"gitlab.com/autom8.network/go-a8-util/fqn"
	pb "gitlab.com/autom8.network/go-a8-util/proto"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

var errNoFunc = errors.New("a function with that name does not exist")
var errNoNS = errors.New("a namespace with that name does not exist")

//getNamespaceInfoCommand gives the correct level of detail about a namespace
func (fn *Fn) getNamespaceInfoCommand(showRequest *pb.ShowRequest) (*pb.A8Response_Show, error) {
	ns := showRequest.Ns

	fqnArray := strings.Split(ns, ".")

	pieces := len(fqnArray)

	var returnMessage = pb.ShowResponse{}.Response
	var err error

	switch pieces {
	case 2:
		returnMessage, err = fn.detailShowResponse(ns)
	case 1:
		appName := fqnArray[0]
		if appName != "" {
			returnMessage, err = fn.listFunctionsShowResponse(fqnArray[0])
		} else {
			returnMessage, err = fn.listAppsShowResponse()
		}

	default:
		err = fmt.Errorf("namespace is too long, it has %d parts and can only have a max of 2", pieces)
	}

	if err != nil {
		return nil, err
	}

	return &pb.A8Response_Show{
		Show: &pb.ShowResponse{
			Response: returnMessage,
		},
	}, nil
}

func (fn *Fn) deployFunctionCommand(deploy *pb.FnData) (*pb.A8Response_Deploy, error) {
	deployResponse, err := fn.deployFunction(deploy)

	if err != nil {
		return nil, err
	}

	return &pb.A8Response_Deploy{
		Deploy: deployResponse,
	}, nil
}

func (fn *Fn) invokeFunctionCommand(invokeReq *pb.InvokeRequest) (*pb.A8Response_Invoke, error) {
	body, err := fn.invokeFunction(invokeReq.Fqn, invokeReq.Input)
	if err != nil {
		return nil, err
	}

	return &pb.A8Response_Invoke{
		Invoke: &pb.InvokeResponse{
			Data: *body,
		},
	}, nil
}

func (fn *Fn) loggerFunctionCommand(request *pb.LoggerRequest) error {
	sysLogUrl := request.SyslogUrl
	appName := request.AppName

	appID, err := fn.getAppIDFromName(appName)
	if err != nil {
		return err
	}

	url := "/v2/apps/"+appID

	fnStringData := `{
		"syslog_url": "tcp://`+sysLogUrl+`"
	}`

	_, err = fn.send(url, http.MethodPut, []byte(fnStringData))

	//responseData := appsPostTransform(body)

	return err
}

func (fn *Fn) getAppIDFromName(appName string) (string, error) {
	fnPath := "/v2/apps?per_page=100&name=" + appName

	body, err := fn.send(fnPath, http.MethodGet, nil)
	if err != nil {
		return "", err
	}

	appData := appsTransform(body)

	if len(appData.Items) > 1 {
		return "", errors.New("too many apps with this name exist")
	}

	if len(appData.Items) == 0 {
		return "", errNoNS
	}

	return appData.Items[0].ID, nil
}

func (fn *Fn) getFunctionIDFromName(appName string, functionName string) (string, error) {
	appID, err := fn.getAppIDFromName(appName)
	if err != nil {
		return "", err
	}

	url := "/v2/fns?app_id=" + appID + "&per_page=2&name=" + functionName

	body, err := fn.send(url, http.MethodGet, nil)
	if err != nil {
		return "", errors.New("failed to get functions list")
	}

	fnData := fnsTransform(body)

	if len(fnData.Items) > 1 {
		return "", errors.New("too many functions with this name exist")
	}

	if len(fnData.Items) == 0 {
		return "", errNoFunc
	}

	return fnData.Items[0].ID, nil

}

//listApps returns a list of fn apps
func (fn *Fn) listApps() ([]appData, error) {

	cursor := ""
	var appData []appData

	for {
		appDataPage, err := fn.getAppPage(cursor)
		if err != nil {
			return nil, err
		}

		appData = append(appData, appDataPage.Items...)

		//if there's more to get, go on to the next page, otherwise exit
		if appDataPage.NextCursor != "" {
			cursor = appDataPage.NextCursor
		} else {
			//if there's nothing more to get break from for loop
			break
		}
	}

	return appData, nil
}

//listFunctionsShowResponse converts a list of functions to ShowResponse type
func (fn *Fn) listAppsShowResponse() (*pb.ShowResponse_NamespaceList, error) {
	appList, err := fn.listApps()
	if err != nil {
		return nil, err
	}

	var nsList []*pb.NamespaceData

	//make an audit function
	for _, appData := range appList {
		nsData := &pb.NamespaceData{
			NamespaceName: appData.Name,
			Audit:         fn.makeAudit(appData.CreatedAt, appData.UpdatedAt),
		}

		nsList = append(nsList, nsData)
	}

	return &pb.ShowResponse_NamespaceList{
		NamespaceList: &pb.NamespaceList{List: nsList},
	}, nil
}

func (fn *Fn) getAppPage(cursor string) (*returnListApps, error) {
	fnPath := "/v2/apps?per_page=100&cursor=" + cursor

	body, err := fn.send(fnPath, http.MethodGet, nil)
	if err != nil {
		return nil, err
	}

	return appsTransform(body), err
}

// listFunctions returns a list of fn functions for a given appName
func (fn *Fn) listFunctions(appName string) ([]fnData, error) {
	appID, err := fn.getAppIDFromName(appName)
	if err != nil {
		return nil, errNoNS
	}

	cursor := ""
	var functionsData []fnData

	for {
		fnDataPage, err := fn.getFunctionsPage(appID, cursor)
		if err != nil {
			return nil, err
		}

		functionsData = append(functionsData, fnDataPage.Items...)

		//if there's more to get, go on to the next page, otherwise exit
		if fnDataPage.NextCursor != "" {
			cursor = fnDataPage.NextCursor
		} else {
			//if there's nothing more to get break from for loop
			break
		}
	}

	return functionsData, nil
}

//listFunctionsShowResponse converts a list of functions to ShowResponse type
func (fn *Fn) listFunctionsShowResponse(appName string) (*pb.ShowResponse_NamespaceList, error) {
	functionList, err := fn.listFunctions(appName)
	if err != nil {
		return nil, err
	}

	var nsList []*pb.NamespaceData

	//make an audit function
	for _, fnData := range functionList {
		nsData := &pb.NamespaceData{
			NamespaceName: fnData.Name,
			Audit:         fn.makeAudit(fnData.CreatedAt, fnData.UpdatedAt),
		}

		nsList = append(nsList, nsData)
	}

	return &pb.ShowResponse_NamespaceList{
		NamespaceList: &pb.NamespaceList{List: nsList},
	}, nil
}

// getFunctionsPage gets one page of functions from Fn
func (fn *Fn) getFunctionsPage(appID string, cursor string) (*returnListFns, error) {

	s := []string{"/v2/fns", "?", "app_id", "=", appID, "&", "per_page", "=", "100", "&", "cursor", "=",  cursor}
	var url = strings.Join(s, "")

	body, err := fn.send(url, http.MethodGet, nil)
	if err != nil {
		return nil, errors.New("failed to get functions list")
	}

	return fnsTransform(body), err
}

func (fn *Fn) getFunctionDetail(fullName string) (*fnData, error) {
	appName, fnName, err := fqn.Parse(fullName)
	if err != nil {
		return nil, err
	}

	fnDataList, err := fn.listFunctions(appName)
	if err != nil {
		return nil, err
	}

	for _, fnData := range fnDataList {
		if fnData.Name == fnName {
			return &fnData, nil
		}
	}

	return nil, errors.New("no function found by this name")
}

func (fn *Fn) detailShowResponse(fqn string) (*pb.ShowResponse_FnFunctionData, error) {
	fnDetail, err := fn.getFunctionDetail(fqn)
	if err != nil {
		return nil, err
	}

	return &pb.ShowResponse_FnFunctionData{
		FnFunctionData: &pb.FnData{
			Fqn:         fqn,
			Image:       fnDetail.Image,
			Memory:      int32(fnDetail.Memory),
			Timeout:     int32(fnDetail.Timeout),
			IdleTimeout: int32(fnDetail.IdleTimeout),
			Audit:       fn.makeAudit(fnDetail.CreatedAt, fnDetail.UpdatedAt),
		},
	}, nil
}

func (fn *Fn) makeAudit(created string, updated string) *pb.Audit {
	layout := "2006-01-02T15:04:05.000Z"

	//if time cannot be parsed then use unix time 0
	createdAt, err := time.Parse(layout, created)
	if err != nil {
		createdAt = time.Unix(0, 0)
	}

	updatedAt, err := time.Parse(layout, updated)
	if err != nil {
		createdAt = time.Unix(0, 0)
	}

	return &pb.Audit{
		CreatedAt: createdAt.Unix(),
		UpdatedAt: updatedAt.Unix(),
	}
}

// createApp creates a new fn app of a given naem
func (fn *Fn) createApp(appName string) (*appData, error) {
	pa := postApp{
		Name: appName,
	}

	data, _ := json.Marshal(pa)

	body, err := fn.send("/v2/apps", http.MethodPost, data)
	if err != nil {
		return nil, err
	}

	return appsPostTransform(body), err
}

// DeployFunction either updates or creates a function on FN. It will create any missing apps necessary
func (fn *Fn) deployFunction(deploy *pb.FnData) (*pb.FnData, error) {
	appName, functionName, err := fqn.Parse(deploy.Fqn)
	if err != nil {
		return nil, err
	}

	_, err = fn.getFunctionIDFromName(appName, functionName)

	if err == errNoFunc || err == errNoNS {
		return fn.deployNewFunction(deploy)
	}

	if err != nil {
		return nil, err
	}

	return fn.deployExistingFunction(deploy)
}

func (fn *Fn) deployNewFunction(deploy *pb.FnData) (*pb.FnData, error) {
	appName, fnName, err := fqn.Parse(deploy.Fqn)
	if err != nil {
		return nil, err
	}

	appID, err := fn.getOrCreateAppID(appName)
	if err != nil {
		return nil, err
	}

	dps := deployPostSend{}

	dps.Name = fnName
	dps.AppID = appID
	dps.Image = deploy.Image
	dps.Memory = int(deploy.Memory)
	dps.Timeout = int(deploy.Timeout)
	dps.IdleTimeout = int(deploy.IdleTimeout)

	data, _ := json.Marshal(dps)

	a8util.Log.WithFields(a8util.Fields{
		"appID": appID,
	}).Info("trying to deploy a new function")

	s := []string{"/v2/fns", "?", "app_id", "=", appID}
	var url = strings.Join(s, "")

	body, err := fn.send(url, http.MethodPost, data)

	if err != nil {
		return nil, err
	}

	drd, err := deployTransform(body)
	if err != nil {
		return nil, err
	}

	return &pb.FnData{
		Fqn:         deploy.Fqn,
		Image:       drd.Image,
		Memory:      int32(drd.Memory),
		Timeout:     int32(drd.Timeout),
		IdleTimeout: int32(drd.IdleTimeout),
		Audit:       fn.makeAudit(drd.CreatedAt, drd.UpdatedAt),
	}, nil
}

func (fn *Fn) deployExistingFunction(deploy *pb.FnData) (*pb.FnData, error) {
	a8util.Log.Info("in put updates")

	appName, functionName, err := fqn.Parse(deploy.Fqn)
	if err != nil {
		return nil, err
	}

	appID, err := fn.getOrCreateAppID(appName)
	if err != nil {
		return nil, err
	}

	fnID, err := fn.getFunctionIDFromName(appName, functionName)
	if err != nil {
		return nil, err
	}

	dus := deployUpdateSend{}

	dus.Timeout = int(deploy.Timeout)
	dus.Memory = int(deploy.Memory)
	dus.Image = deploy.Image
	dus.IdleTimeout = int(deploy.IdleTimeout)

	data, err := json.Marshal(dus)
	if err != nil {
		return nil, err
	}

	s := []string{"/v2/fns/", fnID, "?", "app_id", "=", appID}
	var url = strings.Join(s, "")

	body, err := fn.send(url, http.MethodPut, data)
	if err != nil {
		return nil, err
	}

	drd, err := deployTransform(body)
	if err != nil {
		return nil, err
	}

	return &pb.FnData{
		Fqn:         deploy.Fqn,
		Image:       drd.Image,
		Memory:      int32(drd.Memory),
		Timeout:     int32(drd.Timeout),
		IdleTimeout: int32(drd.IdleTimeout),
		Audit:       fn.makeAudit(drd.CreatedAt, drd.UpdatedAt),
	}, nil
}

func (fn *Fn) getOrCreateAppID(appName string) (string, error) {
	// if we have an appID just get it
	appID, err := fn.getAppIDFromName(appName)
	if err == nil {
		return appID, nil
	}

	appData, _ := fn.createApp(appName)

	a8util.Log.WithFields(a8util.Fields{
		"id":      appData.ID,
		"name":    appData.Name,
		"created": appData.CreatedAt,
		"updated": appData.UpdatedAt,
	}).Info("creating a new app")

	return appData.ID, nil
}

// invokeFunction is the start of the request to fnsserver for invoke
func (fn *Fn) invokeFunction(FQN string, data []byte) (*json.RawMessage, error) {
	appName, functionName, err := fqn.Parse(FQN)
	if err != nil {
		return nil, err
	}

	appID, err := fn.getAppIDFromName(appName)
	if err != nil {
		return nil, err
	}

	fnID, err := fn.getFunctionIDFromName(appName, functionName)
	if err != nil {
		return nil, err
	}

	s := []string{"/invoke/", fnID, "?", "app_id", "=", appID}
	var url = strings.Join(s, "")

	body, err := fn.send(url, http.MethodPost, data)

	jsonBody := json.RawMessage(body)

	return &jsonBody, err
}

func (fn *Fn) send(url string, method string, dataRaw []byte) ([]byte, error) {
	if fn.Domain == "" || fn.Port <= 0 || url == "" || method == "" {
		a8util.Log.WithFields(a8util.Fields{
			"domain": fn.Domain,
			"port":   fn.Port,
			"url":    url,
			"method": method,
		}).Panic("invalid HTTPRequestBuilder configuration")
	}

	client := &http.Client{}
	var body []byte

	var domain string
	if domain = fn.Domain; fn.Port != 80 {
		s := []string{fn.Domain, ":", strconv.Itoa(fn.Port)}
		domain = strings.Join(s, "")
	}

	s := []string{fn.Protocol, "://", domain, url}
	var fullURL = strings.Join(s, "")

	a8util.Log.WithFields(a8util.Fields{
		"url":        fullURL,
		"httpMethod": method,
		"data":       a8util.TruncateString(string(dataRaw), 1000),
	}).Trace("Making fn call")

	var buf = bytes.NewBuffer(dataRaw)

	req, err := http.NewRequest(method, fullURL, buf)
	if err != nil {
		return nil, err
	}

	resp, err := client.Do(req)

	if err != nil {
		var errorMsg = "No fn Server ready"

		return nil, errors.New(errorMsg)
	}

	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}
