package fnConnect

import "testing"

func TestDeployTransform(t *testing.T) {
	rawData := "{\"id\":\"01DDJ54XPNNGBG00RZJ0000002\",\"name\":\"helloworld\",\"app_id\":\"01DDJ54XJYNGBG00RZJ0000001\",\"image\":\"testnetfns.a8.dev/samtest4.helloworld:0.0.4\",\"memory\":128,\"timeout\":30,\"idle_timeout\":30,\"created_at\":\"2019-06-17T07:24:14.421Z\",\"updated_at\":\"2019-06-17T08:07:25.711Z\"}"
	_, err := deployTransform([]byte(rawData))

	if err != nil {
		t.Errorf("err could unmarshal %s", err)
	}
}
