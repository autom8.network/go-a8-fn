module gitlab.com/autom8.network/go-a8-fn

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/sendgrid/rest v2.4.1+incompatible // indirect
	github.com/sendgrid/sendgrid-go v3.4.1+incompatible // indirect
	github.com/sirupsen/logrus v1.4.1
	github.com/urfave/cli v1.20.0 // indirect
	gitlab.com/autom8.network/go-a8-http v0.0.0-20190618141447-2303c420574e
	gitlab.com/autom8.network/go-a8-util v0.0.0-20190624211909-8b2090c44ebe
)

// replace gitlab.com/autom8.network/go-a8-http => ../go-a8-http

// replace gitlab.com/autom8.network/go-a8-util => ../go-a8-util
