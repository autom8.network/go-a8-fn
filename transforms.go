package fnConnect

import (
	"encoding/json"

	a8util "gitlab.com/autom8.network/go-a8-util"
)

func appsTransform(rawData []byte) *returnListApps {
	var rla returnListApps
	if err := json.Unmarshal(rawData, &rla); err != nil {
		a8util.Log.Error("Failed to unmarshal data")
	}

	a8util.Log.WithFields(a8util.Fields{
		"stringData": a8util.TruncateString(string(rawData), 1000),
		"rla.Items":  len(rla.Items),
	}).Info("tyring to unmarshal data")

	return &rla
}

func fnsTransform(rawData []byte) *returnListFns {
	var rlf returnListFns
	if err := json.Unmarshal(rawData, &rlf); err != nil {
		a8util.Log.Error("Failed to unmarshal data")
	}

	return &rlf
}

func appsPostTransform(rawData []byte) *appData {
	var ad appData
	if err := json.Unmarshal(rawData, &ad); err != nil {
		a8util.Log.Error("Failed to unmarshal data")
	}

	return &ad
}

func deployTransform(rawData []byte) (*deployReturnData, error) {
	a8util.Log.Info("deployTransformDataRaw")
	a8util.Log.Info(string(rawData))

	var deploy deployReturnData

	if err := json.Unmarshal(rawData, &deploy); err != nil {
		a8util.Log.Error("Failed to unmarshal data in deployTransform")
		return nil, err
	}

	return &deploy, nil
}
